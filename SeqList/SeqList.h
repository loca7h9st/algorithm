//
// Created by root on 2024/5/10.
//

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLDataType;

typedef struct SeQlist {
    SLDataType* arr;
    int size;
    int capacity;
}SL;


void SLInit(SL* ps);
void SLDestroy(SL* ps);
void SLdisplay(SL* ps);
void SLBackpush(SL* ps , SLDataType x);
void SLBackpop(SL* ps );
void SLFrontpush(SL* ps , SLDataType x);
void SLFrontpop(SL* ps);