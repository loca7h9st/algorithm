给定一个数组nums, 编写一个函数将所有0移动到数组的末尾，同时保持非零元素的相对顺序。

请注意，必须在不复制数组的情况下原地对数组进行修改。

e.g.

```c
输入 nums = [0, 1, 0, 3, 12]
输出 [1, 3, 12, 0, 0]
```

![image-20240424140925022](https://gitee.com/rio-root/picgo/raw/master/img/image-20240424140925022.png)

```c
void swap(int* x, int* y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void moveZeroes(int* nums, int numsSize) {
    int dest = -1;
    int curr = 0;
    for (int i = 0; i < numsSize; i++) {
        if (nums[curr] == 0) {
            ++curr;
        } else {
            swap(&nums[++dest], &nums[curr]);
            ++curr;
        }
    }
}
```

