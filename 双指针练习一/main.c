#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>

void swap(int* x, int* y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

int main()
{
	int arr[] = { 0, 1, 0, 3, 12 };
	int dest = -1;
	int curr = 0;
	int length = sizeof(arr) / sizeof(arr[0]);

	for (int i = 0; i < length; i++)
	{
		if (arr[curr] == 0)
		{
			++curr;
		}
		else
		{
			swap(&arr[++dest], &arr[curr]);
			++curr;
		}
	}

	for (int i = 0; i < length; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}