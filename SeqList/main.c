#include "SeqList.h"
//
// Created by root on 2024/5/10.
//
int main() {
    SL s;
    SLInit(&s);
    for (int i = 1; i < 10; ++i) {
        SLFrontpush(&s, i);
    }
    // for (int i = 1; i < 10; ++i) {
    //     SLFrontpop(&s);
    // }
    SLdisplay(&s);
    SLDestroy(&s);
}