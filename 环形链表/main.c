/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */
struct ListNode *detectCycle(struct ListNode *head) {
    struct ListNode* slow = head, *fast = head;
    while(fast != NULL){
        slow = slow->next;
        if(fast->next == NULL){
            return NULL;
        }
        fast = fast->next->next;
        if(slow == fast){
            struct ListNode* ptr = head;
            while( ptr != slow){
                slow = slow->next;
                ptr = ptr->next;
            }
            return ptr;
        }
    }
    return NULL;
}