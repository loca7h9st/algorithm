![image-20240425155204821](https://gitee.com/rio-root/picgo/raw/master/img/image-20240425155204821.png)

```c
#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#define LEN(arr) (sizeof(arr) / sizeof(arr[0]))

void duplicateZeros(int* arr, int arrSize) {
    int curr = 0;
    int prve = -1;
    while (curr < arrSize)
    {
        // 判断当前元素是否等于0，如果等于0将curr指针向前移动两步，否则移动一步
        prve++;
        if (arr[prve] != 0)
        {
            curr++;
        }
        else
        {
            curr += 2;
        }

    }
    // 记录当前数组最后一个索引值
    int j = arrSize - 1;
    // 如果curr 累加到了该索引长度就将最后一个元素赋值为0，比如[1, 0, 2, 3, 0, 4]
    if (curr == arrSize + 1)
    {
        arr[j] = 0;
        j--;
        prve--;
    }
    while (j >= 0)
    {
        arr[j] = arr[prve]; 
        j--;
        if (!arr[j])
        {
            arr[j] = arr[prve];
            j--;
        }
        prve--;
    }
}


int main() {
    int arr[] = { 1, 0, 2, 3, 0, 4, 5, 0 };
    int length = LEN(arr);
    duplicateZeros(arr, length);
    for (int i = 0; i < length; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}

```

