#define _CRT_SECURE_NO_WARNINGS 1

#include <stdbool.h>
#include <stdio.h>


int bigSum(int n)
{
    int sum = 0;
    while (n) {
        int t = n % 10;
        sum += t * t;
        n /= 10;
    }
    return sum;
}

_Bool isHappy(int n) {
    int slow = n, fast = bigSum(n);
    while (slow != fast) {
        slow = bigSum(slow);
        fast = bigSum(bigSum(fast));
    }
    return slow == 1;
 }
