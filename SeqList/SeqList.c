//
// Created by root on 2024/5/10.
//

#include "SeqList.h"

void SLInit(SL* ps) {
    assert(ps);
    ps->arr = (SLDataType*)malloc(sizeof(SLDataType) * 4);
    if (ps->arr == NULL) {
        perror("malloc fail");
        return;
    }
    ps->size = 0;
    ps->capacity = 4;
}

void SLDestroy(SL* ps) {
    free(ps->arr);
    ps->arr = NULL;
    ps->capacity = ps->size = 0;
}
void SLdisplay(SL* ps) {
    for (int i = 0; i < ps->size; ++i) {
        printf("%d ", ps->arr[i]);
    }
    printf("\n");
}

void SLBackpush(SL* ps , SLDataType x) {
    assert(ps);
    if (ps->size == ps->capacity) {
        SLDataType* tmp = (SLDataType*) realloc(ps->arr, sizeof(SLDataType) * ps->capacity * 2); // 如果空间不足，则在原来的大小上翻倍
        if (tmp == NULL) {
            perror("realloc fail");
        }
        ps->arr = tmp; // 把翻倍的空间还给SL
        ps->capacity *= 2; // 记得容量也要翻倍
    }
    ps->arr[ps->size++] = x;
}

void SLBackpop(SL* ps ) {
    assert(ps->size > 0);
    ps->size--;
}


void SLFrontpush(SL* ps , SLDataType x) {
    assert(ps);
    if (ps->size == ps->capacity) {
        SLDataType* tmp = (SLDataType*) realloc(ps->arr, sizeof(SLDataType) * ps->capacity * 2); // 如果空间不足，则在原来的大小上翻倍
        if (tmp == NULL) {
            perror("realloc fail");
        }
        ps->arr = tmp; // 把翻倍的空间还给SL
        ps->capacity *= 2; // 记得容量也要翻倍
    }
    //int end = ps->size - 1;
    // while (end>=0) {
    //     ps->arr[end+1] = ps->arr[end];
    //     --end;
    // }
    // ps->arr[0] = x;
    // ps->size++;
    for (int i = ps->size; i > 0; --i) {
        ps->arr[i] = ps->arr[i-1];
    }
    ps->arr[0] = x;
    ps->size++;
}
void SLFrontpop(SL* ps) {
    assert(ps);
    assert(ps->size);
    for (int i = 0; i < ps->size; ++i) {
        ps->arr[i] = ps->arr[i+1];
    }
    ps->size--;
}