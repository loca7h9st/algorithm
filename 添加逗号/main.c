#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>



void reverse_string(char* str) {
	int i = 0, j = 0;
	char temp;
	// 计算字符串长度
	while (str[j] != '\0') {
		j++;
	}
	j--; // j 现在指向字符串的最后一个字符

	// 交换字符以反转字符串
	while (i < j) {
		temp = str[i];
		str[i] = str[j];
		str[j] = temp;
		i++;
		j--;
	}
}

void format_number(int n, char* buffer) {
	int digit_count = 0;
	int index = 0;
	if (n == 0) {
		buffer[index++] = '0';
	}
	else {
		while (n > 0)
		{
			if (digit_count == 3)
			{
				buffer[index++] = ',';
				digit_count = 0;
			}
			buffer[index++] = (n % 10) + '0';
			n /= 10;
			digit_count++;
		}
	}
	buffer[index] = '\0';
	reverse_string(buffer);
}


int main() {
	int n = 9803657; // 输入的整数
	char formatted_number[50]; // 存储格式化后的结果

	format_number(n, formatted_number); // 格式化数字
	printf("Formatted number: %s\n", formatted_number); // 打印格式化后的数字

	return 0;
}