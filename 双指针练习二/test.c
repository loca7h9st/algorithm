#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#define LEN(arr) (sizeof(arr) / sizeof(arr[0]))

void duplicateZeros(int* arr, int arrSize) {
    int curr = 0;
    int prve = -1;
    while (curr < arrSize)
    {
        prve++;
        if (arr[prve] != 0)
        {
            curr++;
        }
        else
        {
            curr += 2;
        }

    }
    int j = arrSize - 1;
    if (curr == arrSize + 1)
    {
        arr[j] = 0;
        j--;
        prve--;
    }
    while (j >= 0)
    {
        arr[j] = arr[prve]; 
        j--;
        if (!arr[j])
        {
            arr[j] = arr[prve];
            j--;
        }
        prve--;
    }
}


int main() {
    int arr[] = { 1, 2, 0, 1 };
    int length = LEN(arr);
    duplicateZeros(arr, length);
    for (int i = 0; i < length; i++) {
        printf("%d ", arr[i]);
    }
    return 0;
}
