#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_N 10000000

int prime[MAX_N]; // 用于存储所有的素数
int isPrime[MAX_N]; // 标记是否为素数，0表示不是素数，1表示是素数

// 使用欧拉筛法计算1到n之间所有的素数
void EulerSieve(int n) {
    int pCount = 0; // 记录素数的个数
    memset(isPrime, 1, sizeof(isPrime)); // 初始化所有数为可能的素数
    isPrime[0] = isPrime[1] = 0; // 0和1不是素数

    for (int i = 2; i <= n; i++) {
        if (isPrime[i]) { // 如果i是素数
            prime[pCount++] = i; // 存储i到prime数组中
        }
        for (int j = 0; j < pCount && i * prime[j] <= n; j++) {
            isPrime[i * prime[j]] = 0; // 标记i * prime[j]为非素数
            if (i % prime[j] == 0) { // 关键步骤：确保每个合数只被它的最小素因数筛除
                break;
            }
        }
    }

    printf("Total primes up to %d: %d\n", n, pCount);
}

int main() {
    int n = 1000000; // 你可以调整这个数值以测试更大或更小的范围
    EulerSieve(n);
    return 0;
}
